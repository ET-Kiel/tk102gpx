
FILES=tk102-1

TIMELIMIT=

all: $(patsubst %, %.gpx, $(FILES))

%.gpx: %.log
	./tk102gpx.py $(TIMELIMIT) $< > $@

nc%: tk102-%.log
	./tk102cat.py -p 1020$* | tee -a $< | cat -v

tail%: tk102-%.log
	./tk102gpx.py $(TIMELIMIT) -CFf $< | tee tk102-$*.txt

%.txt: %.log
	./tk102gpx.py -CF $< > $@

%.png: %.log
	gnuplot -c tk102.gpt $*
