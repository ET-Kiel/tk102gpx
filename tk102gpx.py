#! /usr/bin/python3

import sys, re, time
import gpxpy
import gpxpy.gpx
from datetime import datetime, UTC
from getopt import getopt

options, files = getopt(sys.argv[1:],
                        "qvft:s:RDCGXF",
                        ["quiet", "verbose", "follow", "time=", "serial=",
                         "raw", "data", "cooked", "gnuplot", "gpx", "fieldnames"])

follow = False
serial = b"0"
verbose = 1
raw = 0
fieldnames = False

for o,v in options:
    o = " "+o
    if o in " -q --quiet":
        verbose = 0
    if o in " -v --verbose":
        verbose += 1
    if o in " -f --follow":
        follow = True
    if o in " -D --data":
        raw = 1
    if o in " -C --cooked":
        raw = 2
    if o in " -G --gnuplot":
        raw = 3
    if o in " -R --raw":
        raw = 4
    if o in " -X --gpx":
        raw = 0
    if o in " -F --fieldnames":
        fieldnames = True
    if o in " -s --serial":
        serial = v.encode()
    if o in " -t --time":
        if v[-1]=="s":
            t = float(v[:-1])
        elif v[-1]=="m":
            t = float(v[:-1])*60
        elif v[-1]=="h":
            t = float(v[:-1])*3600
        elif v[-1]=="d":
            t = float(v[:-1])*86400
        elif v[-1]=="w":
            t = float(v[:-1])*86400*7
        else:
            t = float(v)
        serial = time.strftime("%y%m%d%H%M", time.gmtime(time.time()-t)).encode()
        if verbose:
            print(f"--serial={serial}", file=sys.stderr)
"""
2403182137,<E9><F0><8F><CC>^]<CB><FE><CD>g<EE>^]}<FE><FD><DC>_<FF>P,GPRMC,213722.000,A,5416.3748,N,00952.1328,E,0.01,0.00,180324,,,A*6C,F,imei:013777002752663,110<D1>i
"""
tk_re = re.compile(rb"".join((
    rb"(?P<SERIAL>2[4-9][0-9]+),",
    rb"(?P<PHONE>[^,]+(,[^,]*)?),",
    rb"GPRMC,",
    rb"(?P<TIME>[0-9]{6}(\.[0-9]+)?),",
    rb"(?P<AV>[AV]),",
    rb"(?P<LAT>[0-9]+\.[0-9]+),",
    rb"(?P<NS>[NS]),",
    rb"(?P<LON>[0-9]+\.[0-9]+),",
    rb"(?P<EW>[EW]),",
    rb"(?P<SOG>[0-9]+\.[0-9]+)?,",
    rb"(?P<COG>[0-9]+\.[0-9]+)?,",
    rb"(?P<DATE>[0-9]{6}),",
    rb"(?P<VAR>[^,]+)?,",
    rb"(?P<WHAT>[^,]*,)?",
    rb"A\*(?P<GPCKS>[0-9A-F][0-9A-F]),",
    rb"(?P<SIGNAL>[FL])?,",
    rb"(?P<MESSAGE>[^,]+,)?",
    rb"imei:(?P<IMEI>[0-9]+),",
    rb"(?P<LEN>[0-9]{3})",
    rb"(?P<BCKS>2[^234]|[^2][^2])?"
)), flags=re.DOTALL)

def gprmc2deg(d,dd):
    s = -1 if dd in b'WS' else 1
    f = float(d)
    d = int(f) // 100
    m = f - 100*d
    return s*(d+m/60)

class TKGPX(gpxpy.gpx.GPX):

    def tk_newtrack(self, files):
        if files:
            fn = files[0]
            files[:1] = []
            inp = open(fn, "rb")
        else:
            fn = None
            inp = sys.stdin.buffer
        track = gpxpy.gpx.GPXTrack(name=fn)
        self.tracks.append(track)
        segment = gpxpy.gpx.GPXTrackSegment()
        track.segments.append(segment)
        return inp, segment

    def __init__(self, files):

        global fieldnames

        super().__init__()
        inp, segment = self.tk_newtrack(files)

        slept = False
        data = inp.read(0x100000)
        while data or follow or files:
            m = tk_re.search(data)
            while not m:
                if verbose >= 3:
                    print(len(data), repr(data[-80:]))
                ndata = inp.read(0x100000)
                while not ndata and files:
                    inp.close()
                    imp, segment = self.tk_newtrack(files)
                    ndata = inp.read(0x100000)
                if not ndata:
                    break
                data += ndata
                m = tk_re.search(data)
            if m is None:
                if not follow:
                    break
                sys.stdout.flush()
                try:
                    if verbose >= 2:
                        slept = True
                        sys.stderr.write(".")
                        sys.stderr.flush()
                    time.sleep(10)
                except KeyboardInterrupt:
                    break
                continue
            if slept:
                sys.stderr.write("\n")
                slept = False

            if m.start() and verbose >= 2:
                print(f"discarded: {repr(data[:m.start()])}", file=sys.stderr)
            data = data[m.end():]
            rd = m.groupdict()

            if not rd["LAT"] or not rd["LON"] or rd["SERIAL"] < serial:
                if verbose >= 3:
                    print(f"DROPPED: {repr(rd)}", file=sys.stderr)
                continue

            if verbose >= 3:
                print(repr(rd), file=sys.stderr)

            if verbose>0 and rd["SIGNAL"] != b"F":
                print(f"Signal is {repr(rd['SIGNAL'])} at {rd['SERIAL']}", file=sys.stderr)

            if verbose>0 and rd["MESSAGE"]:
                print(f"MESSAGE is {repr(rd['MESSAGE'])} at {rd['SERIAL']}", file=sys.stderr)

            pd = {
                "latitude": gprmc2deg(rd["LAT"], rd["NS"]),
                "longitude": gprmc2deg(rd["LON"], rd["EW"]),
            }
            if rd["TIME"] and rd["DATE"]:
                pd["time"] = datetime(
                    int(rd["DATE"][4:6])+2000,
                    int(rd["DATE"][2:4]),
                    int(rd["DATE"][0:2]),
                    int(rd["TIME"][0:2]),
                    int(rd["TIME"][2:4]),
                    int(rd["TIME"][4:6]),
                    0,
                    UTC
                )
            if rd["SOG"]:
                pd["speed"] = float(rd["SOG"]) # knots

            if verbose == 2:
                print(repr(pd), file=sys.stderr)

            if raw==1:
                if fieldnames:
                    print(", ".join([f"{k}={pd[k]}" for k in pd]))
                else:
                    print(" ".join([f"{pd[k]}" for k in pd]))

            if raw==2:
                cd = {}
                cd.update(strorbytes(rd, 'SIGNAL'))
                cd.update(strorbytes(rd, 'SERIAL'))
                if rd['PHONE'].isascii():
                    cd.update(strorbytes(rd, 'PHONE'))
                else:
                    cd['PHONE'] = "*"*14
                cd['time'] = pd['time'].isoformat()[:19]+'Z'
                cd['lat'] = f"{rd['LAT'][:2].decode()}°{rd['LAT'][2:].decode()}'{rd['NS'].decode()}"   
                cd['lon'] = f"{rd['LON'][:3].decode()}°{rd['LON'][3:].decode()}'{rd['EW'].decode()}"   
                cd.update(strorbytes(rd, 'SOG'))
                cd.update(strorbytes(rd, 'COG'))
                if rd['MESSAGE']:
                    cd.update(strorbytes(rd, 'MESSAGE'))
                if fieldnames:
                    print(", ".join([f"{k}={cd[k]}" for k in cd]))
                else:
                    print(" ".join([cd[k] for k in cd]))

            if raw==3:
                if fieldnames:
                    print("timestr time lat log")
                    fieldnames = False
                print(" ".join([
                    f"{pd['time'].isoformat()+'Z'}",
                    f"{pd['time'].timestamp():.0f}",
                    f"{pd['latitude']:.6f}",
                    f"{pd['longitude']:.6f}"]))
                
            if raw==4:
                if fieldnames:
                    print(", ".join([f"{k}={repr(rd[k])[2:-1]}" for k in rd if rd[k]]))
                else:
                    print(" ".join([f"{repr(rd[k])[2:-1]}" for k in rd if rd[k]]))

            point = gpxpy.gpx.GPXTrackPoint(**pd)
            segment.points.append(point)

            if follow and not raw and verbose < 2:
                print(repr(point), file=sys.stderr)

        if verbose > 1:
            sys.stdout.flush()
            print(f"Unprocessed data: {len(data)} Bytes: {repr(data)}", file=sys.stderr)

def strorbytes(rd, f):
    if not f in rd:
        return {}
    try:
        return {f: rd[f].decode()}
    except:
        pass
    return {f: repr(rd[f])[2:-1]}

# gpxpy.gpx.GPXTrackPoint(
#     latitude: Optional[float] = None,
#     longitude: Optional[float] = None,
#     elevation: Optional[float] = None,
#     time: Optional[datetime.datetime] = None,
#     symbol: Optional[str] = None,
#     comment: Optional[str] = None,
#     horizontal_dilution: Optional[float] = None,
#     vertical_dilution: Optional[float] = None,
#     position_dilution: Optional[float] = None,
#     speed: Optional[float] = None,
#     name: Optional[str] = None,
# ) -> None

X = TKGPX(files)
if not raw:
    print(X.to_xml())
