#! /usr/bin/python3

import os, sys, time, select, socket, getopt, fcntl

port = 10201
out = sys.stdout.buffer
files = []
timeout = None

def logger(s):
    print("\n"+s, file=sys.stderr)

oo,files = getopt.getopt(sys.argv[1:], "p:", ("port=",))

for o,v in oo:
    if o=="-p" or o=="--port":
        port = int(v,0)

poll = select.poll()

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(("", port))
sock.listen(2)
poll.register(sock, select.POLLIN)
logger(f"listening on port {port}, socket fd {sock.fileno()}")

class tksocks:
    def __init__(self):
        self.sockets = {}
        self.fdescs = []
    def add(self, s):
        if len(self.fdescs) > 9:
            self.close(self.fdescs[0])
        fd = s[0].fileno()
        self.sockets[fd] = s
        self.fdescs.append(fd)
        poll.register(fd, select.POLLIN | select.POLLHUP | select.POLLRDHUP)
    def get(self, fd):
        return self.sockets[fd]
    def close(self, fd):
        s = self.get(fd)
        logger(f"closing connection from {repr(s[1])}")
        del self.sockets[fd]
        self.fdescs.remove(fd)
        poll.unregister(fd)
        s[0].close()

sockets = tksocks()
        
while True:
    try:
        c = poll.poll(timeout)
        for fd, ev in c:
            if fd==sock.fileno():
                s = sock.accept()
                logger(f"connection from {repr(s[1])}")
                sockets.add(s)
            else:
                if ev & select.POLLIN:
                    d = sockets.get(fd)[0].recv(4096, socket.MSG_DONTWAIT)
                    if d:
                        out.write(d)
                        out.flush()
                if ev & (select.POLLHUP | select.POLLRDHUP):
                    sockets.close(fd)
    except KeyboardInterrupt:
        break
    except Exception as e:
        logger(f"Error: {repr(e)}")
